# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [GNU rush])
m4_define([AT_PACKAGE_TARNAME],   [rush])
m4_define([AT_PACKAGE_VERSION],   [2.2])
m4_define([AT_PACKAGE_STRING],    [GNU rush 2.2])
m4_define([AT_PACKAGE_BUGREPORT], [bug-rush@gnu.org])
