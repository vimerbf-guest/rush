rush

  This package provides a shell '/usr/sbin/rush' with a need for
  the 'set-uid' capability. The reason is that the shell executable
  maintains book-keeping in the UTMP file '/var/lib/rush/utmp'.
  The information can not be written when the shell only gains the
  underprivileged access of a local user. Once the accounting files
  have been updated, GNU Rush will fall back to the personality of
  the calling user.

  At first thought one might consider registering '/usr/sbin/rush'
  as a valid shell. However, the intention behind GNU Rush is to
  let an administrator have the powers to govern very accurately
  which services a user is granted access to on the local host.
  Thus the registering of this shell would have the drawback that
  any user on the system will be able to use chsh(1) to assign
  himself access to '/usr/sbin/rush', thereby gaining resource
  access that an administrator in all probability intends for
  only a select number of local users. In addition, there are
  services, like some FTP servers, that use entries in '/etc/shells'
  as a guarantee that a user be entitled to use specific services.
  This is a further case to beware of. Together these considera-
  tions motivated the package maintainer not to register GNU Rush
  as a shell, that is, at the time of package installation.

  The present package has pushed the original upstream configuration
  file into the documentation directory:

        /usr/share/doc/rush/examples/rush.rc

  Instead the maintainer has produced a more extended file in

        /etc/rush.rc

  which is more in accordance with Debian practice. It contains
  functional settings for

        scp, sftp, rsync, svn, cvs, and git.

  The way that '/etc/rush.rc' is set up, it will allow users to
  execute commands like the following:

      $ scp file dumle@host:/incoming/ftp/      ## Only uploads

      $ sftp dumle@host

      $ rsync -Ca www dumle@host:~/

      $ svn info svn+ssh://dumle@host/repos-name/

      $ git clone git+ssh://dumle@host/gitroot/repos.git

      $ cvs -d :ext:dumle@host/srv/cvsroot/ co module

  These require that the chroot directory '/srv/rush/' has
  been properly populated with suitable binaries, and with the
  relevant repositories or sub-directories!


Important point for GNU/kFreeBSD
================================

  Device files like '$chrootdir/dev/null' can freely be created
  under GNU/Linux, at least if the mount point supports devices.

  In GNU/kFreeBSD the situation is different: an instance of
  'devfs' must be instantiated.

     #  mount_devs  devfs  $chrootdir/dev/

  Many binaries demand the presence of '$chrootdir/dev/null',
  so be prepared to execute that mount call when using this
  software with Debian GNU/kFreeBSD.


On chrooting a service under GNU Rush.
======================================

  For a services like 'sftp' and 'scp' it is preferable to arrange
  a chrooted environment, into which GNU Rush will lock the user.
  Three such setups are illustrated here.

  1. Take 'sftp' as an example. A commented-out configuration in
     '/etc/rush.rc' suggests a setting

     rule sftp-rush
       command ^.*/sftp-server
       uid >= 1000
       set[0] /usr/bin/sftp-server
       umask 002
       chroot /srv/rush
       chdir ~

     Here '/srv/rush/' would need to be a directory sufficiently
     rich to allow chrooting into. Only the execution of
     '/usr/bin/sftp-server', i.e., running the binary
     '/srv/rush/usr/bin/sftp-server', need to be arranged.

     The generation of such a minimal chroot-able directory can be
     accomplished with

        /usr/share/doc/rush/scripts/mkchroot-rush.pl

     which carries its own documentation. The simplest possible call

        # perl mkchroot-rush.pl --user dumle \
                     --chrootdir /srv/rush \
                     --tasks etc,dev,home,bin,lib \
                     --binaries /usr/lib/sftp-server

     populates a directory for the above rule 'sftp-rush'.

  2. Each user could have his own individual chroot directory:

     rule sftp-rush
       command ^.*/sftp-server
       uid >= 1000
       set[0] /usr/bin/sftp-server
       umask 002
       chroot ~
       chdir ~

     If the home directory is named as '/home/dumle' in '/etc/passwd',
     then the above rule would chroot the user 'dumle' into '/home/dumle',
     and will initially position him at '/home/dumle/home/dumle' when
     he issues the command "sftp dumle@server".

     A minimal chroot directory with 'sftp'server' is in this case
     easily generated using

        # perl mkchroot-rush.pl --user dumle \
                     --chroot /home/dumle \
                     --tasks etc,dev,home,bin,lib \
                     --binaries /usr/lib/sftp-server

     The complication here is that the call must be repeated for
     each user, replacing both strings "dumle" with the appropriate
     user name. Recall also that each user must be defined on the
     server itself!

  3. A third possibility would be to use 'debootstrap' to install
     a complete chroot directory. In this case, the path
     '/usr/bin/sftp-server' used above, would need to be changed.
     The home directory could be added using

        # perl mkchroot-rush.pl --user dumle \
                     --chroot /srv/rush \
                     --tasks etc,home \
                     --force

     Here '--force' accepts the fact that '/srv/rush' already exists.
     The corresponding construct is the setting for 'sftp-rush' in the
     prepackaged '/etc/rush.rc'.


Service notification
====================

  This software offers the possibility to use the TCPMUX
  service to send notifications or execute other accounting
  tasks, triggered by the completion of a user task, i.e.,
  at his log-off time.

  For this to work an administrator can use 'xinetd' or
  'inetutils-inetd' as super-server, the legacy server
  'openbsd-inetd' can not be used as it does not provide
  the necessary TCPMUX service. The file

     /usr/share/doc/rush/examples/rush-notifier

  and either of

     /usr/share/doc/rush/examples/rush.xinetd
     /usr/share/doc/rush/examples/rush.inetutils-inetd

  together provide a correctly implemented notification subsystem.
  Some further comments are contained in each of these files.

  To fully activate this service, there is an inserted line

     #post-socket inet://localhost

  in the default rule of '/etc/rush.rc'. This has to be uncommented.
  Failing this, or failing to let the chosen 'inetd' reload the updated
  configuration, GNU Rush will not be able to activate the notification
  mailer.

  Further details on this mechanism are introduced in the GNU Rush
  TeXinfo pages (delivered in a separate package)

     '4.2.7 Notification', and

     '6.7 Note notification example'.
