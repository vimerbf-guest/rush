Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GNU rush
Upstream-Contact: Sergey Poznyakoff <gray@gnu.org.ua>
Source: https://puszcza.gnu.org.ua/projects/rush/

Files: *
Copyright: 2008-2016, Sergey Poznyakoff <gray@gnu.org.ua>
License: GPL-3+

Files: build-aux/* gnu/* m4/*
Copyright: 1992-2016, Free Software Foundation, Inc.
License: GPL-3+

Files: build-aux/mdate-sh
Copyright: 1995-2015, Free Software Foundation, Inc.
           1995, Ulrich Drepper <drepper@gnu.ai.mit.edu>
License: GPL-2+

Files: build-aux/missing
Copyright: 1996-2013, Free Software Foundation, Inc.
           1996, Fran,cois Pinard <pinard@iro.umontreal.ca>
License: GPL-2+

Files: po/*.po po/rush.pot
Copyright: 2010-2014, Free Software Foundation, Inc.
           2009-2016, Sergey Poznyakoff <gray@gnu.org>
           2009-2010, Clytie Siddall <clytie@riverland.net.au>
           2016, Trần Ngọc Quân <vnwildman@gmail.com>
           2010-2016, Koen Torfs <koen@indigetesdii.org>
           2014, Mario Blättermann <mario@blaettermann@gmail.com>
           2010-2011, Jorma Karvonen <karvonen.jorma@gmail.com>
           2013-2014, Rémy Chevalier <remychevalier@laposte.net>
           2015, Stéphane Aulery <lkppo@free.fr>
           2013, Leandro Regueiro <leandro.regueiro@gmail.com>
           2014-2016, Rafael Fontenelle <rffontenelle@gmail.com>
           2012, Мирослав Николић <miroslavnikolic@rocketmail.com>
License: GPL-3+

Files: po/Makefile.in.in
Copyright: 1995-1997, 2000-2007, 2009-2010, Ulrich Drepper <drepper@gnu.ai.mit.edu>
Comment: Makefile for PO directory in any package using GNU gettext.
 Origin is gettext-0.17.
License: custom-gettext
 This file can be copied and used freely without restrictions.  It can
 be used in projects which are not available under the GNU General Public
 License but which still want to provide support for the GNU gettext
 functionality.
 Please note that the actual code of GNU gettext is covered by the GNU
 General Public License and is *not* in the public domain.

Files: lib/Makefile.in
Copyright: 1994-2013, Free Software Foundation, Inc.
           2008-2016, Sergey Poznyakoff <gray@gnu.org.ua>
License: GPL-3+

Files: etc/Makefile.in src/Makefile.in
Copyright: 1994-2013, Free Software Foundation, Inc.
           2008-2016, Sergey Poznyakoff <gray@gnu.org.ua>
License: GPL-3+

Files: doc/*.1 doc/*.5
Copyright: 2016, Sergey Poznyakoff <gray@gnu.org.ua>
License: GPL-3+

Files: debian/*
Copyright: 2022, Bo YU <tsu.yubo@gmail.com>
           2010-2016, Mats Erik Andersson <mats.andersson@gisladisker.se>
License: GPL-3+
Comment: This package was initially debianized by Mats Erik Andersson
 on Tue, 2 Feb 2010 22:18:00 +0100, then gradually refined.

Files: build-aux/install-sh
Copyright: 1994, X Consortium
 2010-2016, Free Software Foundation
Comment: The changes to this file by FSF are in the public domain.
License: custom-X
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
 .
 FSF changes to this file are in the public domain.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 On Debian GNU/Linux systems as well as on Debian GNU/kFreeBSD systems,
 the complete text of the GNU General Public License (GPL) can be found
 in the file "/usr/share/common-licenses/GPL-3".
